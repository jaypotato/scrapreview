@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="bgc-white bd bdrs-3 p-20 mB-20">
                <div class="col-sm-6">
                <span>
                    Showing Review of
                    <h4>{{ $item_name }}</h1>
                </span>
                    
                </div>
                <table id="reviewsTable" class="table table-bordered table-hover" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <td>#</td>
                            <!-- <td>Product ID/ASIN</td> ?
                            <td>Title</td> ?
                            <td>Body</td>   ?
                            <td>Score</td> v ?
                            <td>Date</td> v
                            <td>Author</td> v
                            <td>Comments Count</td> v
                            <td>Has Image</td> v
                            <td>Has Video</td> v
                            <td>Helpful Count</td> v
                            <td>Verified</td> v -->
                            <td>Review</td>
                            <!-- <td>Created At</td>
                            <td>Updated At</td>  -->
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('datatableStyles')
    <link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection

@section('datatableScripts')
    <script src="https://datatables.yajrabox.com/js/jquery.min.js"></script>
    <script src="https://datatables.yajrabox.com/js/bootstrap.min.js"></script>
    <script src="https://datatables.yajrabox.com/js/jquery.dataTables.min.js"></script>
@endsection

@section('datatables')
    <script type="text/javascript">
        
        $(function() {
            $('#reviewsTable').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
                ajax: '{{ route('review.data', $unique_job_id) }}',
                columns: [
                    { data: 'DT_Row_Index', orderable: false, searchable: false},
                    { 
                        data: null,
                        render: function(data) {
                            if(data.has_image == 1) 
                                var has_image = 'Yes';
                            else var has_image = 'No';
                            
                            if(data.has_video == 1) 
                                var has_video = 'Yes';
                            else var has_video = 'No';

                            if(data.is_verified == 1) 
                                var verified = 'Verified : Yes';
                            else var verified = 'Verified : No';

                            var stars_icons = "";
                            for(var i=0; i<5; i++) {
                                if(data.score >= i+1)
                                    stars_icons += "<i class=\"glyphicon glyphicon-star\"></i>";
                                else stars_icons += "<i class=\"glyphicon glyphicon-star-empty\"></i>";
                                // console.log(["i" => i, ]);
                            };
                            
                            return '<div class="row"><a href="'+data.link+'">'+data.title+'</a></div>'+
                            '<div class="row">'+data.body+'</div>'+
                            '<div class="row">'+
                                '<div class="col-sm-4">'+stars_icons+data.score+'</div>'+
                                '<div class="col-sm-4">Written By : <a href="'+data.author_link+'">'+data.author_name+'</a></div>'+
                            '</div>'+
                            '<div class="row">'+
                                '<div class="col-sm-4">'+data.helpful_count+' people find this helpful</div>'+
                                '<div class="col-sm-4">'+data.comments_count+' Comments</div>'+
                                '<div class="col-sm-4">'+verified+'</div>'+
                            '</div>'+
                            '<div class="row">'+
                                '<div class="col-sm-4">Has Image : '+has_image+'</div>'+
                                '<div class="col-sm-4">Has Video : '+has_video+'</div>'+
                                '<div class="col-sm-4">Review Date : '+data.date+'</div>'+
                            '</div>'+
                            '<div class="row">'+
                                '<div class="col-sm-4">Variation Detail | '+data.variation+'</div>'+
                            '</div>' +
                            '<div class="row">'+
                                '<div class="col-sm-4">Child/Variation ASIN : '+data.child_asin+'</div>'+
                            '</div>' 
                        }
                    },
                    // { data: 'created_at'},
                    // { data: 'updated_at' },
                ],
            });
        });    
    </script>
@endsection