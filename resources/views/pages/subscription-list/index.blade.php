@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="bgc-white bd bdrs-3 p-20 mB-20">
                <table id="subsTable" class="table table-bordered table-hover" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <td>#</td>
                            <td>ASIN</td>
                            <td>Link</td>                        
                            <td>Status</td>
                            <td>Initiated at</td>
                            <td>Updated at</td>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('datatableStyles')
    <link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection

@section('datatableScripts')
    <script src="https://datatables.yajrabox.com/js/jquery.min.js"></script>
    <script src="https://datatables.yajrabox.com/js/bootstrap.min.js"></script>
    <script src="https://datatables.yajrabox.com/js/jquery.dataTables.min.js"></script>
@endsection

@section('datatables')
    <script type="text/javascript">
        $(function() {
            $('#subsTable').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
                ajax: '{!! route('subs.data') !!}',
                columns: [
                    { data: 'DT_Row_Index', orderable: false, searchable: false},
                    { data: 'asin' },
                    { 
                        data: 'unique_job_id',
                        searchable: false,
                        render: function(data){
                            return '<a href="http://localhost:4346/dashboard/review/'+data+'">Get Data</a>';
                        }
                    },
                    { data: 'status' },
                    { data: 'created_at'},
                    { data: 'updated_at' },
                ],
            });
        });    
    </script>
@endsection
