@extends('layouts.app')
@section('content')
    <div class="jumbotron">
        <h1>Amazon Review Sucker Tool</h1>
        <p>This tool will scrap review information of any amazon product. Input your product ID/ASIN, this tool will crawling and scrapping information then store it to database.</p>
        <p></p>
        <form method="post" action="{{route('scrap.start')}}" enctype="multipart/form-data">
        @csrf
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="icon icon-search"></i>
                </span>
                <input type="text" name="asin" class="form-control" aria-label="Search" placeholder="Product ASIN ...">
            </div>
                <button class="btn btn-primary" type="submit">                    
                    Search
                </button>
        </form>
        
    </div>
@endsection
