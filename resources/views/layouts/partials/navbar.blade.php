<div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="true">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a href="https://transferwise.com" class="logo-text logo-text-inverse hidden-md hidden-lg hidden-xl"><span class="sr-only">TransferWise</span></a>
                <a href="https://transferwise.com" class="navbar-brand visible-md visible-lg visible-xl">TransferWise</a>
            </div>
            <ul class="nav navbar-nav pull-xs-right">
              <li class="dropdown m-r-1">
                <a href="" class="dropdown-toggle profile-name" style="color: white;"data-toggle="dropdown" role="button" aria-expanded="false">
                  <span class="text-ellipsis" title="Customer name">
                    <div class="circle circle-sm circle-inverse">
                      <img class="profile-name__avatar--img" src="https://bootstrap.transferwise.com/assets/img/mike.jpg" alt="Customer name">
                    </div><span class="hidden-xs hidden-sm m-l-1">Customer name</span>
                  </span>
                  <span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-xs-right dropdown-menu-md dropdown" role="menu">
                  <li class="p-a-2">
                    <div class="media">
                      <div class="media-left">
                        <img class="dropdown-menu__avatar--img circle circle-sm" src="https://bootstrap.transferwise.com/assets/img/mike.jpg" alt="Customer name">
                      </div>
                      <div class="media-body">
                        <div class="small text-muted">
                          Membership number P5065117
                        </div>
                        <a href="/user/profile/view" class="dropdown-menu__view-profile-link small">
                          <span>
                            View personal profile
                          </span>
                        </a>
                      </div>
                    </div>
                  </li>
                  <li class="divider"></li>

                  <li class="nav__dropdown-menu-items">
                    <a href="/user/profile/verification">
                      <i class="icon icon-verification"></i>
                      <span>Verification</span>
                    </a>
                  </li>
                  <li class="nav__dropdown-menu-items">
                    <a href="/user/settings">
                      <i class="icon icon-setting"></i>
                      <span>Settings</span>
                    </a>
                  </li>
                  <li class="nav__dropdown-menu-items">
                    <a href="{{ route('logout') }}" 
                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                      <i class="icon icon-logout"></i>
                      <span>Log out</span>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: one;">
                        {{ csrf_field() }}
                    </form>
                  </li>
                </ul>
              </li>
            </ul>

            <nav id="navbar" class="navbar-collapse navbar-collapse-with-panel collapse" aria-expanded="false">
                <a class="cover" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false"></a>
                <div class="navbar-collapse-panel">
                    <ul class="nav navbar-nav">
                        <!-- <li class="dropdown">
                            <button class="dropdown-toggle">Dropdown button<span class="caret"></span></button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here <span class="badge badge-success">new</span></a></li>
                            </ul>
                        </li> -->
                        <li class="dropdown navbar-collapse-flatten">
                            <button class="dropdown-toggle">Menu Navigation<span class="caret"></span></button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ route('dashboard.index') }}">Dashboard</a></li>
                                <li><a href="{{ route('subscription-list.index') }}">Review List</a></li>
                                <!-- <li><a href="#">Something else here <span class="badge badge-success">new</span></a></li> -->
                            </ul>
                        </li>
                    </ul>
                    <!-- <div class="navbar-right order-1">
                        <ul class="nav navbar-nav">
                          <li class="dropdown">
                              <button class="dropdown-toggle">Dropdown button<span class="caret"></span></button>
                              <ul class="dropdown-menu" role="menu">
                                  <li><a href="#">Action</a></li>
                                  <li><a href="#">Another action</a></li>
                                  <li><a href="#">Something else here <span class="badge badge-success">new</span></a></li>
                              </ul>
                          </li>
                          <li class="active"><a href="https://transferwise.com/">Link</a></li>
                        </ul>
                    </div> -->
                </div>
                <a role="button" class="navbar-toggle close" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="true">×</a>
            </nav>
        </div>