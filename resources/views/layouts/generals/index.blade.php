@extends('layouts.app')

 @section('content')
    @slot('body')
        @yield('inner-content')
    @endslot
 @endsection