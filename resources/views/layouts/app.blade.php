<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <link rel="icon" href="{{URL::to('bootstrap/docs/favicon.ico')}}">
    <title>Nested navbar Template for Bootstrap</title>
    <link href="{{URL::to('bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
</head>

<body>

    <header class="navbar navbar-inverse bg-primary navbar-static-top circle-bg-header affix-top" data-spy="affix" data-offset-top="60">
        @include('layouts.partials.navbar')
    </header>

    <div class="main-content">
        <div class="container">
           @yield('content')
        </div>
    </div>
    @yield('datatableStyles')
    @yield('datatableScripts')
    @yield('datatables')
    
    
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
</body>

</html>
