<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Redirect::to('/login');
});

Auth::routes();

Route::group(['prefix' => 'dashboard', 'middleware' => 'auth'], function(){
    Route::get('/', 'HomeController@index')->name('dashboard.index');
    Route::post('scrap', 'ScrapController@doScrap')->name('scrap.start');
    Route::resource('subscription-list', 'SubscriptionListController', ['except' => ['show']]);
    Route::get('subscription-list/data', 'SubscriptionListController@data')->name('subs.data');
    Route::get('review/{link}', 'ReviewController@getReview')->name('review.get');
    Route::get('review/{link}/data', 'ReviewController@data')->name('review.data');
});


// Route::get('/home', 'HomeController@index')->name('home');
