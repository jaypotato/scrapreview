<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subscription;
use App\Review;
use App\User;
use DB;
use Yajra\Datatables\Datatables;
use View;

class ReviewController extends Controller
{
    public function getReview($unique_job_id)
    {   
        $item_name = Subscription::where('unique_job_id', $unique_job_id)->first()->item_name;
        
        // return response()->view()->compact('link', 'item_name');
        return View::make('pages.subscription-list.review', compact('unique_job_id', 'item_name'));
    }

    public function data($unique_job_id)
    {
        $reviews = DB::table('review')->join('subscription', function ($join) use($unique_job_id){
          $join->on('review.ASIN', '=', 'subscription.asin') 
            ->where('subscription.unique_job_id', $unique_job_id)->select('review.*','subscription.unique_job_id');
        })->get();

        return Datatables::of($reviews)->addIndexColumn()->make(TRUE);
    }
}
