<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\ProcessScrapping;
use App\Subscription;

class ScrapController extends Controller
{
    /**
     * Pushing scrap job to queue
     *
     * @return response
     */
    public function doScrap(Request $request)
    {
        $unique_job_id = base_convert(microtime(false), 10, 36);

        $subs = new Subscription();
        $subs->user_id = \Auth::user()->id;
        $subs->asin = $request->asin;
        $subs->link = $unique_job_id; //this serve as unique job id and '/link' that navigates to page after crawling process has finished
        $subs->status = 'processing';
        $subs->save();

        ProcessScrapping::dispatch($request->asin, $unique_job_id)
            ->delay(now()->addMinutes(0.2));

    
        return redirect()->route('subscription-list.index');
    }
}
