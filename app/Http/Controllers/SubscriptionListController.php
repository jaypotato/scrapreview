<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Review;
use App\Subscription;
use Yajra\Datatables\Datatables;

class SubscriptionListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->view('pages.subscription-list.index');
    }

    /**
     * Fetch data using ajax
     * 
     * @return \App\Subscription
     */
    public function data()
    {
        $subs = Subscription::where('user_id', \Auth::user()->id)->get();
        return Datatables::of($subs)->addIndexColumn()->make(TRUE);
        // $subs = Subscription::select(['asin', 'link', 'status', 'created_at', 'updated_at']);
        // return Datatables::of($subs)->addIndexColumn()->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
