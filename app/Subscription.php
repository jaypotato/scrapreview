<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $table = 'subscription';
    protected $fillable = ['user_id', 'asin', 'unique_job_id', 'status', 'item_name'];

    public function User()
    {
        $this->belongsTo('App\User');
    }

    public function Review()
    {
        $this->belongsTo('App\Review');
    }
}
