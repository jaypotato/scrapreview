<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scrapped extends Model
{
    protected $table = 'scrapped';
    protected $fillable = ['asin', 'review_count'];

}
