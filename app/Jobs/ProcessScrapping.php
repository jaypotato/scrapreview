<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Goutte\Client;
use App\Review;
use App\Scrapped;
use App\Subscription;

class ProcessScrapping implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $asin;
    protected $unique_job_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($asin, $unique_job_id)
    {
        $this->asin = $asin;
        $this->unique_job_id = $unique_job_id;
    }

    /**
     * Execute the crawl and scrap job.
     *
     * @return void
     */
    public function handle()
    {
        $asin = $this->asin;
        $unique_job_id = $this->unique_job_id;
        // $is_exist = Scrapped::where('asin', $asin)->get();
        //check whether this product is scrapped before
        //yes, scrapped before.
        // if(!$is_exist->isEmpty()) 
        // {
        //     $client = new Client();
        //     $crawler = $client->request('GET', 'https://www.amazon.com/product-reviews/'.$asin.'/ref=cm_cr_getr_d_paging_btm_prev_1?ie=UTF8&sortBy=recent&pageNumber=1&reviewerType=all_reviews');
        //     $review_count = floatval(str_replace(',','',$crawler->filter('.totalReviewCount')->text()));

        //     //check if this product has more review than before
        //     //yes
        //     if($review_count > $is_exist->review_count)
        //     {
        //         //tbd : get new data only; $scrap_from_page = ;

        //         //scrap the product.
        //         $review_pages = ceil($review_count / 10);
        //         for($i=0; $i<$review_pages; $i++)
        //         {
        //             if($i !== 0 && $i < $review_pages) 
        //             {
        //                 $page = $i+1;
        //                 $crawler = $client->request('GET', 'https://www.amazon.com/product-reviews/'.$asin.'/ref=cm_cr_getr_d_paging_btm_prev_1?ie=UTF8&sortBy=recent&pageNumber='.$page.'&reviewerType=all_reviews');
        //             }
        //             $crawler->filter('.a-section > .review')->each(function ($node) use($asin)
        //             {
        //                 $review = new Review();
        //                 $review->ASIN = $asin;
        //                 $review->review_id = $node->attr('id');
        //                 $review->title = $node->filter('.review-title')->text();
        //                 $review->link = 'https://www.amazon.com'. $node->filter('div > a')->attr('href');
        //                 $review->body = $node->filter('.review-text')->text();
        //                 $review->score = floatval(substr($node->filter('div > a')->attr('title'), 0, 3));
        //                 $review->date = date_create_from_format(" F j, Y",  (substr($node->filter('.review-date')->text(), 2)));
        //                 $review->author_name = $node->filter('.author')->text();
        //                 $review->author_link =  'https://www.amazon.com'. $node->filter('.author')->attr('href');
        //                 $review->comments_count = intval($node->filter('.review-comment-total')->text());
        //                 try{
        //                     $node->filter('.review-image-container > div > img')->text();
        //                     $review->has_image = 1;
        //                 } catch(\InvalidArgumentException $e) {
        //                     $review->has_image = 0;
        //                 }
    
        //                 try{
        //                     $node->filter('.video-block')->text();
        //                     $review->has_video = 1;
        //                 } catch(\InvalidArgumentException $e) {
        //                     $review->has_video = 0;
        //                 }
                        
        //                 try{
        //                     $helpful_count = $node->filter('.cr-vote-text')->text();
        //                     echo(strpos($helpful_count, 'One') !==false);
        //                     if(strpos($helpful_count, 'One') !== false)
        //                         $review->helpful_count = 1;
        //                     else
        //                         $review->helpful_count = explode(" ",$helpful_count)[0]; 
                                
        //                 } catch(\InvalidArgumentException $e) {
        //                     $review->helpful_count = 0;
        //                 }
                        
        //                 try{
        //                     $node->filter('div > span > a');
        //                     $review->is_verified = 1;
        //                 } catch(\InvalidArgumentException $e) {
        //                     $review->is_verified = 0;
        //                 }
        //                 /*
        //                     tbd : make rollback case, when there's any error(s) occurs, 
        //                         no review of [product ID]  will be saved.  
        //                 */
        //                 $review->save();
        //             });
        //         }
        //     }

        //     //no
        //     else
        //     {
        //         // //return data from db
        //         // $data = Review::where('asin', $asin)->get();
        //         // return view($data);
        //         //return flag=done.
        //     }
        // }

        //no, haven't scrap this product yet
        //else 
        //{
            $client = new Client();
            $crawler = $client->request('GET', 'https://www.amazon.com/product-reviews/'.$asin.'/ref=cm_cr_getr_d_paging_btm_prev_1?ie=UTF8&sortBy=recent&pageNumber=1&reviewerType=all_reviews');
            $review_count = floatval(str_replace(',','',$crawler->filter('.totalReviewCount')->text()));

            $sub = Subscription::where('unique_job_id', $unique_job_id)->first();
            $sub->item_name = $crawler->filter('.product-title > h1 > a')->text();
            $sub->save();

            $review_pages = ceil($review_count / 10);
            for($i=0; $i<$review_pages; $i++)
            {
                if($i !== 0 && $i < $review_pages) 
                {
                    $page = $i+1;
                    $crawler = $client->request('GET', 'https://www.amazon.com/product-reviews/'.$asin.'/ref=cm_cr_getr_d_paging_btm_prev_1?ie=UTF8&sortBy=recent&pageNumber='.$page.'&reviewerType=all_reviews');
                }
                $crawler->filter('.a-section > .review')->each(function ($node) use($asin)
                {
                    $review = new Review();
                    $review->ASIN = $asin;
                    $review->review_id = $node->attr('id');
                    $review->title = $node->filter('.review-title')->text();
                    $review->link = 'https://www.amazon.com'. $node->filter('div > a')->attr('href');
                    $review->body = $node->filter('.review-text')->text();
                    $review->score = floatval(substr($node->filter('div > a')->attr('title'), 0, 3));
                    $review->date = date_create_from_format(" F j, Y",  (substr($node->filter('.review-date')->text(), 2)));
                    $review->author_name = $node->filter('.author')->text();
                    $review->author_link =  'https://www.amazon.com'. $node->filter('.author')->attr('href');
                    $review->comments_count = intval($node->filter('.review-comment-total')->text());
                    try{
                        $node->filter('.review-image-container > div > img')->text();
                        $review->has_image = 1;
                    } catch(\InvalidArgumentException $e) {
                        $review->has_image = 0;
                    }

                    try{
                        $node->filter('.video-block')->text();
                        $review->has_video = 1;
                    } catch(\InvalidArgumentException $e) {
                        $review->has_video = 0;
                    }
                    
                    try{
                        $helpful_count = $node->filter('.cr-vote-text')->text();
                        echo(strpos($helpful_count, 'One') !==false);
                        if(strpos($helpful_count, 'One') !== false)
                            $review->helpful_count = 1;
                        else
                            $review->helpful_count = explode(" ",$helpful_count)[0]; 
                            
                    } catch(\InvalidArgumentException $e) {
                        $review->helpful_count = 0;
                    }
                    
                    try{
                        $node->filter('div > span > a');
                        $review->is_verified = 1;
                    } catch(\InvalidArgumentException $e) {
                        $review->is_verified = 0;
                    }
                    /*
                        tbd : make rollback case, when there's any error(s) occurs, 
                            no review of [product ID]  will be saved.  
                    */
                    try{
                        $variation_details = $node->filter('.review-format-strip > a');
                        $url = $variation_details->attr('href');
                        $segments = explode('/', parse_url($url, PHP_URL_PATH));
                        $review->child_asin = $segments[3];
                        $review->variation = $variation_details->text();
                    } catch(\InvalidArgumentException $e) {
                        $review->child_asin = '-';
                        $review->variation = '-';
                    }
                
                    $review->save();
                });
            }
        //}
    }
}
