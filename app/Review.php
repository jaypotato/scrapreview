<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = 'review';
    protected $fillable = ['ASIN', 'review_id', 'title', 'link', 'body', 'score', 'date', 
    'author_name', 'author_link', 'comments_count', 'has_image', 'has_video', 'helpful_count', 'is_verified',
    'child_asin', 'variation'];
    
}
