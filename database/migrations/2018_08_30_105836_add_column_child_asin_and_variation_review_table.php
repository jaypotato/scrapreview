<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnChildAsinAndVariationReviewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('review', function(Blueprint $table){
            $table->string('child_asin')->after('is_verified')->default(NULL);
            $table->text('variation')->after('child_asin')->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('review', function(Blueprint $table){
            $table->dropColumn(['child_asin', 'variation']);
        });
    }
}
