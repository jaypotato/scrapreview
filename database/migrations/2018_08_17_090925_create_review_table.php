<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('review', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ASIN');
            $table->string('review_id');
            $table->string('title');
            $table->string('link');
            $table->text('body');
            $table->float('score');
            $table->date('date');
            $table->string('author_name');
            $table->string('author_link');
            $table->integer('comments_count');
            $table->boolean('has_image')->default(FALSE);
            $table->boolean('has_video')->default(FALSE);
            $table->boolean('is_verified')->default(FALSE);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('review');
    }
}
